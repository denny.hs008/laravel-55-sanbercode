<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Soal Looping</h1>
    <?php
    echo "<h3>Contoh Soal 1</h3>";
    echo "<h5>Looping 1</h5>";


    $i = 2;
    while ($i <= 20) {
        echo $i . " - I Love PHP <br>";
        $i+=2;
    }

    echo "<h5>Looping 2</h5>";

    for($a = 20; $a >= 2; $a-=2){
        echo $a . " - I Love PHP <br>";
    }

    echo "<h3>Soal 2</h3>";

    $nilai = [18, 45, 29, 61, 47, 34];

    echo "array numbers: ";
    print_r($nilai);

    foreach($nilai as $value){
        $rest[] = $value % 5;

    };
    echo "<br>";
    echo "Array sisa bagi 5 adalah: ";
    echo "<br>";
    print_r($rest);

    echo "<h3>Soal 3</h3>";

    $bioItems = [
            ["001", "Keyboard Logitek", 60000, "Keyboard yang mantap untuk kantoran", "logitek.jpeg"], 
            ["002", "Keyboard MSI", 300000, "Keyboard gaming MSI mekanik", "msi.jpeg"],
            ["003", "Mouse Genius", 50000, "Mouse Genius biar lebih pinter", "genius.jpeg"],
            ["004", "Mouse Jerry", 30000, "Mouse yang disukai kucing", "jerry.jpeg"]

    ];

    foreach($bioItems as $indexarray){
        $key = [
            "id" => $indexarray[0],
            "name" => $indexarray[1],
            "price" => $indexarray[2],
            "description" => $indexarray[3],
            "source" => $indexarray[4],
        ];
        print_r($key);
        echo "<br>";

    }

    echo "<h3>Soal 4</h3>";

    // $k = 51
    //

    for ($k=1; $k<=5; $k++){
        for ($l=$k; $l <= 5 ; $l++) {
            echo "*";
        }
        echo "<br>";
    }

    ?>
</body>
</html>