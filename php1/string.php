<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    
    <?php   
        echo "<h3> Soal No 1</h3>";
        $kalimat1 = "PHP is never old";

        echo "Kalimat1 : " . $kalimat1 . "<br>";
        echo "Panjang String Kalimat 1 : " . strlen($kalimat1) . "<br>";
        echo "Jumlah Kata Kalimat 1 : " . str_word_count($kalimat1) . "<br>";

        echo "<h3> Soal No 2</h3>";
        $kalimat2 = "I Love PHP";

        echo "Kata 1 Kalimat 2 : " . substr($kalimat2,0,1) . "<br>";
        echo "Kata 2 Kalimat 2 : " . substr($kalimat2,2,4) . "<br>";
        echo "Kata 3 Kalimat 2 : " . substr($kalimat2,7) . "<br>";

        echo "<h3> Soal No 3</h3>";
        $kalimat3 = "PHP is old but sexy!";

        echo "Kalimat 3 : " . $kalimat3 . "<br>";
        echo "Ganti string kalimat 3 : " . str_replace("sexy","awesome",$kalimat3);


    ?>

